import * as vscode from 'vscode'; 

import SchemaGenerator from './SchemaGenerator';

export function activate(context: vscode.ExtensionContext) {
    const provider = new SchemaGenerator();
	provider.activate(context);
}