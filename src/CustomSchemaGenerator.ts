import * as path from "path";
import { Config, SchemaGenerator } from "ts-json-schema-generator";

export interface CustomConfig extends Config {
    typeFileName?: string;
}

export class CustomSchemaGenerator extends SchemaGenerator {

    protected readonly config?: CustomConfig;

    protected partitionFiles() {
        // eslint-disable-next-line prefer-const
        let { projectFiles, externalFiles } = super.partitionFiles();

        const typeFileName = this.config?.typeFileName;
        if (!typeFileName) return { projectFiles, externalFiles };

        projectFiles = projectFiles.filter(f => this.isSameFile(f.fileName, typeFileName));
        return { projectFiles, externalFiles };
    }

    protected isSameFile(fileNameA: string, fileNameB: string) {
        return path.normalize(fileNameA) === path.normalize(fileNameB);
    }

}